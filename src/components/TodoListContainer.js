import React, { Component } from 'react';
import {
  RefreshControl,
  StyleSheet,
  View,
  Text,
  ScrollView,
} from 'react-native';
import moment from 'moment';
import uuidv4 from 'uuid/v4';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#FFF',
  },
  todo: {
    borderWidth: 1,
    borderColor: '#000',
  }
});

const initialTodos = [
  {
    title: 'Add a new todo',
    complete: false,
    dueDate: moment().toDate(),
    id: uuidv4(),
  },
  {
    title: 'Delete a todo',
    complete: false,
    dueDate: moment().toDate(),
    id: uuidv4(),
  },
  {
    title: 'Complete a todo',
    complete: false,
    dueDate: moment().toDate(),
    id: uuidv4(),
  },
  {
    title: 'Only one component per file',
    complete: false,
    dueDate: moment().toDate(),
    id: uuidv4(),
  },
  {
    title: 'Search Todos',
    complete: false,
    dueDate: moment().toDate(),
    id: uuidv4(),
  },
  {
    title: 'Apply Basic Styles to Component',
    complete: false,
    dueDate: moment().toDate(),
    id: uuidv4(),
  },
  {
    title: 'Save code to server',
    complete: false,
    dueDate: moment().toDate(),
    id: uuidv4(),
  },
];

export default class TodoListContainer extends Component {
  static navigationOptions = {
    title: 'Todos',
  };

  render() {
    return (
      <TodoList todos={initialTodos} />
    )
  }
}


class TodoList extends Component {
  static propTypes = {
    todos: React.PropTypes.array.isRequired,
  };

  constructor(props) {
    super(props)
  }

  render () {
    const { todos } = this.props;

    return (
      <ScrollView style={styles.container}>
        {todos.map(todo => {
          return (
            <View style={styles.todo} key={todo.id}>
              <Text>{todo.title}</Text>
              <Text>{moment(todo.dueDate).format('lll')}</Text>
              <Text>{todo.complete}</Text>
            </View>
          ) 
        })
        }
      </ScrollView>
    );
  }
}
