import React, { Component } from 'react';
import moment from 'moment';
import StackNavigator from './navigators/stack';

export default class extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <StackNavigator
        ref={nav => this.navigator = nav}
      />
    );
  }
}
