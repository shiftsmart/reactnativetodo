import { StackNavigator } from 'react-navigation';

import TodoListContainer from '../components/TodoListContainer';

export default StackNavigator({
  TodoListContainer: { screen: TodoListContainer },
});
