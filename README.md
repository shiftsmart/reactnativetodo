# README #

React Native Todo App

### Setup ###

[Clone](https://_pavanpatel@bitbucket.org/shiftsmart/reactnativetodo.git) Repo Here

run `yarn`

run `react-native run-ios` or `react-native run-android`

Create your own feature branch, and commit code on a separate branch.

Make a Pull Request to master when complete.

### Requirements ###

1. Add a new todo item with a due date
2. Make a todo 'Complete', and display that in UI
3. Delete a todo
4. Style React Components
5. Add Search Functionality to todos by title
6. Mock code to save adding, completing, deleting code to a server

### Rules ###
- Only one React component per file
- Use any state management library you want or store state at component level




